# DashboardDB

## Criação da Base de Dados

1. Com o usuário postgres, execute o aplicativo psql no servidor onde esta instalado o serviço PostgreSQL:
```Bash
$ psql
```
2. Crie um usuario para o aplicativo SIGAC, juntamente com a senha que será utilizada para acesso. Estas credenciais devem ser informadas no arquivo application.properties:
```Bash
postgres=# CREATE ROLE sigac WITH LOGIN PASSWORD 'sigac';
```
3. Em seguida, crie a base de dados que será utilizada pelo aplicativo SIGAC:
```Bash
postgres=# CREATE DATABASE sigac OWNER=sigac ENCODING=UTF8;
```
4. Conecte-se com a base de dados recem criada e crie o schema "sigac" que será utilizado para organizar os objetos de banco de dados utilizados pelo aplicativo:
```Bash
postgres=# \c sigac
sigac=# CREATE SCHEMA auth;
sigac=# CREATE SCHEMA chamados;
```
5. Por fim, ajuste as permissões para que o usuário criado no item 2 possa utilizar o schema:
```Bash
sigac=# ALTER SCHEMA auth OWNER TO sigac;
sigac=# ALTER SCHEMA chamados OWNER TO sigac;
sigac=# GRANT ALL PRIVILEGES ON DATABASE sigac TO sigac;
sigac=# GRANT ALL PRIVILEGES ON SCHEMA sigac TO sigac;
sigac=# \q
```
