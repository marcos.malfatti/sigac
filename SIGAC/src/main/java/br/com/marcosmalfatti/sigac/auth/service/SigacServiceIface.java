/*
 * Copyright (C) 2022 Marcos Malfatti <marcos.malfatti@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package br.com.marcosmalfatti.sigac.auth.service;

import java.util.Optional;

import org.springframework.data.domain.Page;

import br.com.marcosmalfatti.sigac.auth.dto.SigacDtoIface;
import br.com.marcosmalfatti.sigac.auth.dto.SigacFormLstDtoIface;

/**
* @author Marcos Malfatti <marcos.malfatti@gmail.com>
* @version 0.0.1
* @date 2022-06
*/

public interface SigacServiceIface<Dto extends SigacDtoIface<?>, FormDto extends SigacFormLstDtoIface<?>> {
	
	public Optional<Dto> buscarPorId(Long id);
	public Page<Dto> listar(FormDto formDto);
	public Dto adicionar(Dto dto);
	public Optional<Dto> alterar(Long id, Dto dto);
	public Optional<Dto> ativar(Long id);
	public Optional<Dto> desativar(Long id);
	public Boolean eliminar(Long id);
}
