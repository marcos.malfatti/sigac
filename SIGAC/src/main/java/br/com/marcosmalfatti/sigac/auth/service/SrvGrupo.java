/*
 * Copyright (C) 2022 Marcos Malfatti <marcos.malfatti@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package br.com.marcosmalfatti.sigac.auth.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.marcosmalfatti.sigac.auth.dto.Grupo.GrupoDto;
import br.com.marcosmalfatti.sigac.auth.dto.Grupo.GrupoFormLstDto;
import br.com.marcosmalfatti.sigac.auth.persistence.entities.Grupo;
import br.com.marcosmalfatti.sigac.auth.persistence.repositories.RepGrupo;

/**
* @author Marcos Malfatti <marcos.malfatti@gmail.com>
* @version 0.0.1
* @date 2022-06
*/

@Service
@Transactional(readOnly = true, 
			   propagation = Propagation.REQUIRED)
public class SrvGrupo extends SigacAbstractService<Grupo, GrupoDto, GrupoFormLstDto, RepGrupo>{

	@Override
	public Class<GrupoDto> getDtoClass() {
		return GrupoDto.class;
	}
}
