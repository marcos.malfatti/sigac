/*
 * Copyright (C) 2022 Marcos Malfatti <marcos.malfatti@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package br.com.marcosmalfatti.sigac.auth.persistence.entities;

import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import br.com.marcosmalfatti.sigac.auth.persistence.SigacAbstractModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * @author Marcos Malfatti <marcos.malfatti@gmail.com>
 * @version 0.0.1
 * @date 2022-06
 */

@Entity
@Table(schema = "auth", name = "grupo_perfil", indexes = {
		@Index(name = "ixGrupoPerfil_idGrupo", columnList = "id_grupo"),
		@Index(name = "ixGrupoPerfil_idPerfil", columnList = "id_perfil") }, uniqueConstraints = @UniqueConstraint(name = "ckGrupoPerfil_ids", columnNames = {
				"id_grupo", "id_perfil" }))
@SequenceGenerator(schema = "auth", name = "seq_grupo_perfil", sequenceName = "seq_grupo_perfil", initialValue = 1, allocationSize = 1)
@NoArgsConstructor @SuperBuilder @Data @EqualsAndHashCode(callSuper = true)
public class GrupoPerfil extends SigacAbstractModel<GrupoPerfil> {

	private static final long serialVersionUID = 3000815658441149841L;

	@ManyToOne
	@JoinColumn(name = "id_grupo", referencedColumnName = "id", foreignKey = @ForeignKey(name = "fkGrupoPerfil_idGrupo"))
	private Grupo grupo;

	@ManyToOne
	@JoinColumn(name = "id_perfil", referencedColumnName = "id", foreignKey = @ForeignKey(name = "fkGrupoPerfil_idPerfil"))
	private Perfil perfil;

	@Override
	public GrupoPerfil clone() {
		return GrupoPerfil.builder()
				.id(getId())
				.ativo(getAtivo())
				.deleted(getDeleted())
				.grupo(getGrupo().clone())
				.perfil(getPerfil().clone())
				.build();
	}

	@Override
	public void copyFrom(GrupoPerfil model) {
		if (model.getAtivo() != null) setAtivo(model.getAtivo());
		if (model.getGrupo() != null) setGrupo(model.getGrupo().clone());
		if (model.getPerfil() != null) setPerfil(model.getPerfil().clone());
	}
}
