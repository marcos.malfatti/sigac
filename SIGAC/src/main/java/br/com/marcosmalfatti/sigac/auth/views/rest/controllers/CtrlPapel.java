/*
 * Copyright (C) 2022 Marcos Malfatti <marcos.malfatti@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package br.com.marcosmalfatti.sigac.auth.views.rest.controllers;

import java.net.URI;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.marcosmalfatti.sigac.auth.dto.Ordenacao;
import br.com.marcosmalfatti.sigac.auth.dto.Paginacao;
import br.com.marcosmalfatti.sigac.auth.dto.Papel.PapelDto;
import br.com.marcosmalfatti.sigac.auth.dto.Papel.PapelFormLstDto;
import br.com.marcosmalfatti.sigac.auth.service.SrvPapel;

/**
* @author Marcos Malfatti <marcos.malfatti@gmail.com>
* @version 0.0.1
* @date 2022-06
*/

@RestController
@RequestMapping("api/v1/Papeis")
public class CtrlPapel {

	@Autowired
	private SrvPapel srvPapel;
	
	@GetMapping(path="exemplo")
	public ResponseEntity<?> exemplo(){
		PapelFormLstDto retorno = PapelFormLstDto.builder()
									.nome("Exemplo")
									.ativo(true)
									.parcial(false)
									.paginacao(Paginacao.builder()
												.tamanhoPagina(15)
												.pagina(1)
												.build())
									.ordenacao(Ordenacao.builder().build())
									.build();
		return ResponseEntity.ok(retorno);
	}

	@GetMapping(path = "{papelId}")
	public ResponseEntity<?> getPapelPorId(@PathVariable("papelId") Long papelId) {
		Optional<PapelDto> papelDtoOpt = srvPapel.buscarPorId(papelId);
		if (papelDtoOpt.isPresent()) {
			return ResponseEntity.ok(papelDtoOpt.get());
		}
		return ResponseEntity.notFound().build();
	}

	@GetMapping(path = "listaPapeis")
	public ResponseEntity<?> listaPapeis(@RequestBody @Valid PapelFormLstDto papelFormLstDto) {
		Page<PapelDto> papelDtoOpt = srvPapel.listar(papelFormLstDto);
		if (papelDtoOpt.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(papelDtoOpt);
	}

	@PostMapping(path = "adicionar")
	public ResponseEntity<?> adicionaPapel(@RequestBody @Valid PapelDto papelDto, 
			UriComponentsBuilder uriBuilder) {
		papelDto = srvPapel.adicionar(papelDto);
		URI uri = uriBuilder.path("api/v1/Papeis/{id}").buildAndExpand(papelDto.getId()).toUri();
		return ResponseEntity.created(uri).body(papelDto);
	}

	@PutMapping(path = "alterar/{papelId}")
	public ResponseEntity<?> alteraPapel(@PathVariable("papelId") Long papelId, @RequestBody @Valid PapelDto papelDto) {
		Optional<PapelDto> papelDtoOpt = srvPapel.alterar(papelId, papelDto);
		if (papelDtoOpt.isPresent()) {
			return ResponseEntity.ok(papelDtoOpt.get());
		}
		return ResponseEntity.notFound().build();
	}

	@PutMapping(path = "ativar/{papelId}")
	public ResponseEntity<?> ativaPapel(@PathVariable("papelId") Long papelId) {
		Optional<PapelDto> papelDtoOpt = srvPapel.ativar(papelId);
		if (papelDtoOpt.isPresent()) {
			return ResponseEntity.ok(papelDtoOpt.get());
		}
		return ResponseEntity.notFound().build();
	}

	@PutMapping(path = "desativar/{papelId}")
	public ResponseEntity<?> desativaPapel(@PathVariable("papelId") Long papelId) {
		Optional<PapelDto> papelDtoOpt = srvPapel.desativar(papelId);
		if (papelDtoOpt.isPresent()) {
			return ResponseEntity.ok(papelDtoOpt.get());
		}
		return ResponseEntity.notFound().build();
	}

	@DeleteMapping(path = "{papelId}")
	public ResponseEntity<?> eliminaPapel(@PathVariable("papelId") Long papelId) {
		if (srvPapel.eliminar(papelId)) {
			return ResponseEntity.ok(true);
		}
		return ResponseEntity.notFound().build();
	}
}
