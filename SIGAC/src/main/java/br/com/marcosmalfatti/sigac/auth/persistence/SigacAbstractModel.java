/*
 * Copyright (C) 2022 Marcos Malfatti <marcos.malfatti@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package br.com.marcosmalfatti.sigac.auth.persistence;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
* @author Marcos Malfatti <marcos.malfatti@gmail.com>
* @version 0.0.1
* @date 2022-06
*/

@SuppressWarnings("serial")
@MappedSuperclass
@NoArgsConstructor @SuperBuilder @Data 
public abstract class SigacAbstractModel<M extends SigacModelIface<?>>
		implements Serializable, SigacModelIface<M>, Cloneable {

	@Id
	@GeneratedValue(generator = "seq_gen", strategy = GenerationType.SEQUENCE)
	private Long id;
	
	@Column(name = "ativo", columnDefinition = "boolean not null default true")
	private Boolean ativo;

	@JsonIgnore
	@Column(name = "deleted", columnDefinition = "boolean not null default false")
	private Boolean deleted = Boolean.FALSE;

	abstract public M clone();

	abstract public void copyFrom(M model);;
}
