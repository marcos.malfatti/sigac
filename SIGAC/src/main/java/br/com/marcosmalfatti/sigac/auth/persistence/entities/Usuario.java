/*
 * Copyright (C) 2022 Marcos Malfatti <marcos.malfatti@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package br.com.marcosmalfatti.sigac.auth.persistence.entities;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import br.com.marcosmalfatti.sigac.auth.persistence.SigacAbstractModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * @author Marcos Malfatti <marcos.malfatti@gmail.com>
 * @version 0.0.1
 * @date 2022-06
 */

@Entity
@Table(
	schema = "auth", 
	name = "usuario", 
	indexes = { 
		@Index(name = "ixUsuario_Login", columnList = "login"),
		@Index(name = "ixUsuario_Ativo", columnList = "ativo"),
		@Index(name = "ixUsuario_IdEmpresa", columnList = "id_empresa"),
		@Index(name = "ixUsuario_IdUnidade", columnList = "id_unidade"),
		@Index(name = "ixUsuario_Deleted", columnList = "deleted") })
@SQLDelete(sql = "UPDATE auth.usuario SET deleted = true WHERE id=?")
@Where(clause = "deleted=false")
@SequenceGenerator(schema = "auth", name = "seq_usuario", sequenceName = "seq_usuario", initialValue = 1, allocationSize = 1)
@NoArgsConstructor @SuperBuilder @Data @EqualsAndHashCode(callSuper = true)
public class Usuario extends SigacAbstractModel<Usuario> {

	private static final long serialVersionUID = 3204227881059053417L;

	@Column(name = "login", nullable = false)
	private String login;

	@Column(name = "senha", nullable = false)
	private String senha;

	@ManyToOne
	@JoinColumn(name = "id_empresa", referencedColumnName = "id", foreignKey = @ForeignKey(name = "fkUsuario_IdEmpresa"))
	private Empresa empresa;

	@ManyToOne
	@JoinColumn(name = "id_unidade", referencedColumnName = "id", foreignKey = @ForeignKey(name = "fkUsuario_IdUnidade"))
	private UnidadeOrganizacional unidade;

	@Column(name = "ultima_atualizacao", nullable = false)
	private LocalDateTime ultimaAtualizacao;

	@Override
	public Usuario clone() {
		return Usuario.builder()
				.id(getId())
				.login(getLogin())
				.senha(getSenha())
				.empresa(getEmpresa().clone())
				.unidade(getUnidade().clone())
				.ultimaAtualizacao(getUltimaAtualizacao())
				.ativo(getAtivo())
				.deleted(getDeleted())
				.build();
	}

	@Override
	public void copyFrom(Usuario model) {
		if (model.getLogin() != null) setLogin(model.getLogin());
		if (model.getSenha() != null) setSenha(model.getSenha());
		if (model.getUnidade() != null) setUnidade(model.getUnidade().clone());
		if (model.getUltimaAtualizacao() != null) setUltimaAtualizacao(ultimaAtualizacao = model.getUltimaAtualizacao());
		if (model.getEmpresa() != null) setEmpresa( model.getEmpresa().clone());
		if (model.getAtivo()!= null) setAtivo(model.getAtivo());

	}
}
