/*
 * Copyright (C) 2022 Marcos Malfatti <marcos.malfatti@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package br.com.marcosmalfatti.sigac.auth.service;

import static org.springframework.data.domain.ExampleMatcher.GenericPropertyMatchers.contains;
import static org.springframework.data.domain.ExampleMatcher.GenericPropertyMatchers.exact;

import java.util.Optional;
import java.util.function.Function;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.marcosmalfatti.sigac.auth.dto.AbstractDto;
import br.com.marcosmalfatti.sigac.auth.dto.AbstractFormLstDto;
import br.com.marcosmalfatti.sigac.auth.persistence.SigacAbstractModel;

/**
* @author Marcos Malfatti <marcos.malfatti@gmail.com>
* @version 0.0.1
* @date 2022-06
*/

@Service
@Transactional(
        readOnly = true,
        propagation = Propagation.REQUIRED)
public abstract class SigacAbstractService<M extends SigacAbstractModel<M> , 
										   Dto extends AbstractDto<M>, 
										   FormDto extends AbstractFormLstDto<M>,
										   Repo extends JpaRepository<M, Long>> 
							implements SigacServiceIface<Dto, AbstractFormLstDto<M>> {

	@Autowired
	private Repo repositorio;
	private Class<Dto> dtoClass = getDtoClass();
	
	public abstract Class<Dto> getDtoClass();
	
	private Function<M, Dto> modelToDtoConverter = model -> {
		try {
			return dtoClass.getDeclaredConstructor(model.getClass()).newInstance(model);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	};

	@Override
	public Optional<Dto> buscarPorId(Long id) {
		Optional<M> opt = repositorio.findById(id);
		Optional<Dto> retorno = opt.map(modelToDtoConverter);
		return retorno;
	}

	@Override
	public Page<Dto> listar(AbstractFormLstDto<M> formDto) {
		ExampleMatcher matcher;
		if(formDto.getParcial()) {
			matcher = ExampleMatcher
					.matching()
					.withMatcher("nome", contains().ignoreCase());
		} else {
			matcher = ExampleMatcher
					.matching()
					.withMatcher("nome", exact());
		}
		Example<M> exemplo =  Example.of(formDto.toModel(), matcher);
		Sort ordenacao = formDto.getOrdenacao() != null ? formDto.getOrdenacao().toSort() : Sort.unsorted();
		Pageable paginacao = formDto.getPaginacao() != null ? formDto.getPaginacao().toPageable(ordenacao) : PageRequest.of(0, 10, ordenacao);
		Page<M> page = repositorio.findAll(exemplo, paginacao);
		return page.map(modelToDtoConverter);
	}

	@Override
	@Transactional(readOnly = false)
	public Dto adicionar(Dto dto) {
		dto.setId(null);
		M model = dto.toModel();
		model.setDeleted(false);
		model = repositorio.save(model);
		return modelToDtoConverter.apply(model);
	}

	@Override
	@Transactional(readOnly = false)
	public Optional<Dto> alterar(Long id, Dto dto) {
		Optional<M> modelOpt = repositorio.findById(id);
		if(modelOpt.isPresent()) {
			modelOpt.get().copyFrom(dto.toModel());
			repositorio.save(modelOpt.get());
		}
		return modelOpt.map(modelToDtoConverter);
	}

	@Override
	@Transactional(readOnly = false)
	public Optional<Dto> ativar(Long id) {
		Optional<M> modelOpt = repositorio.findById(id);
		if(modelOpt.isPresent()) {
			modelOpt.get().setAtivo(Boolean.TRUE);;
			repositorio.save(modelOpt.get());
		}
		return modelOpt.map(modelToDtoConverter);
	}

	@Override
	@Transactional(readOnly = false)
	public Optional<Dto> desativar(Long id) {
		Optional<M> modelOpt = repositorio.findById(id);
		if(modelOpt.isPresent()) {
			modelOpt.get().setAtivo(Boolean.FALSE);;
			repositorio.save(modelOpt.get());
		}
		return modelOpt.map(modelToDtoConverter);
	}

	@Override
	@Transactional(readOnly = false)
	public Boolean eliminar(Long id) {
		Optional<M> modelOpt = repositorio.findById(id);
		if(modelOpt.isPresent()) {
			repositorio.deleteById(id);
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
	
}
