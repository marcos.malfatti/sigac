/*
 * Copyright (C) 2022 Marcos Malfatti <marcos.malfatti@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package br.com.marcosmalfatti.sigac.auth.dto.PapelPerfil;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import br.com.marcosmalfatti.sigac.auth.dto.Papel.PapelDto;
import br.com.marcosmalfatti.sigac.auth.dto.Perfil.PerfilDto;
import br.com.marcosmalfatti.sigac.auth.persistence.entities.PapelPerfil;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
* @author Marcos Malfatti <marcos.malfatti@gmail.com>
* @version 0.0.1
* @date 2022-06
*/

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor  
public class PapelPerfilDto {

	private Long id;
	
	@NotNull
	@JsonProperty("papel")
	private PapelDto papelDto;

	@NotNull
	@JsonProperty("perfil")
	private PerfilDto perfilDto;
	
	public PapelPerfil toModel() {
		return PapelPerfil.builder()
				.papel(getPapelDto().toModel().clone())
				.perfil(getPerfilDto().toModel().clone())
				.build();
	}
	
}
