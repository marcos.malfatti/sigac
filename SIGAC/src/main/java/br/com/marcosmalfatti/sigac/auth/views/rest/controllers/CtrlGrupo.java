/*
 * Copyright (C) 2022 Marcos Malfatti <marcos.malfatti@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package br.com.marcosmalfatti.sigac.auth.views.rest.controllers;

import java.net.URI;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.marcosmalfatti.sigac.auth.dto.Ordenacao;
import br.com.marcosmalfatti.sigac.auth.dto.Paginacao;
import br.com.marcosmalfatti.sigac.auth.dto.Empresa.EmpresaDto;
import br.com.marcosmalfatti.sigac.auth.dto.Grupo.GrupoDto;
import br.com.marcosmalfatti.sigac.auth.dto.Grupo.GrupoFormLstDto;
import br.com.marcosmalfatti.sigac.auth.service.SrvGrupo;

/**
* @author Marcos Malfatti <marcos.malfatti@gmail.com>
* @version 0.0.1
* @date 2022-06
*/

@RestController
@RequestMapping("api/v1/Grupos")
public class CtrlGrupo {

	@Autowired
	private SrvGrupo srvGrupo;
	
	@GetMapping(path = "exemplo")
	public ResponseEntity<?> exemplo(){
		EmpresaDto empresa = EmpresaDto.builder()
				.id(1L)
				.nome("Empresa Exemplo")
				.ativo(true)
				.build();
		GrupoFormLstDto retorno = GrupoFormLstDto.builder()
									.nome("Exemplo")
									.empresaDto(empresa)
									.descricao("Grupo de Exemplo")
									.gestorEmpresa(Boolean.FALSE)
									.ativo(Boolean.TRUE)
									.parcial(Boolean.FALSE)
									.paginacao(Paginacao.builder()
												.tamanhoPagina(15)
												.pagina(1)
												.build())
									.ordenacao(Ordenacao.builder().build())
									.build();
		return ResponseEntity.ok(retorno);
	}
	
	@GetMapping(path = "porId/{grupoId}")
	public ResponseEntity<?> getGrupoPorId(@PathVariable("grupoId") Long grupoId) {
		Optional<GrupoDto> grupoDtoOpt = srvGrupo.buscarPorId(grupoId);
		if (grupoDtoOpt.isPresent()) {
			return ResponseEntity.ok(grupoDtoOpt.get());
		}
		return ResponseEntity.notFound().build();
	}
	
	@GetMapping(path = "listaGrupos")
	public ResponseEntity<?> listaGrupos(@RequestBody @Valid GrupoFormLstDto grupoFormLstDto) {
		Page<GrupoDto> grupoDtoPg = srvGrupo.listar(grupoFormLstDto);
		if (grupoDtoPg.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(grupoDtoPg);
	}
	
	@PostMapping(path = "adicionar")
	public ResponseEntity<?> adicionaGrupo(@RequestBody @Valid GrupoDto grupoDto,
			UriComponentsBuilder uriBuilder) {
		grupoDto = srvGrupo.adicionar(grupoDto);
		URI uri = uriBuilder.path("api/v1/Grupos/{id}").buildAndExpand(grupoDto.getId()).toUri();
		return ResponseEntity.created(uri).body(grupoDto);
	}
	
	@PutMapping(path = "alterar/{grupoId}")
	public ResponseEntity<?> alteraGrupo(@PathVariable("grupoId") Long grupoId, @RequestBody @Valid GrupoDto grupoDto) {
		Optional<GrupoDto> grupoDtoOpt = srvGrupo.alterar(grupoId, grupoDto);
		if (grupoDtoOpt.isPresent()) {
			return ResponseEntity.ok(grupoDtoOpt.get());
		}
		return ResponseEntity.notFound().build();
	}
	
	@PutMapping(path = "ativar/{grupoId}")
	public ResponseEntity<?> ativaGrupo(@PathVariable("grupoId") Long grupoId) {
		Optional<GrupoDto> grupoDtoOpt = srvGrupo.ativar(grupoId);
		if (grupoDtoOpt.isPresent()) {
			return ResponseEntity.ok(grupoDtoOpt.get());
		}
		return ResponseEntity.notFound().build();
	}
	
	@PutMapping(path = "desativar/{grupoId}")
	public ResponseEntity<?> desativaGrupo(@PathVariable("grupoId") Long grupoId) {
		Optional<GrupoDto> grupoDtoOpt = srvGrupo.desativar(grupoId);
		if (grupoDtoOpt.isPresent()) {
			return ResponseEntity.ok(grupoDtoOpt.get());
		}
		return ResponseEntity.notFound().build();
	}
	
	@DeleteMapping(path = "{grupoId}")
	public ResponseEntity<?> eliminaGrupo(@PathVariable("grupoId") Long grupoId) {
		if (srvGrupo.eliminar(grupoId)) {
			return ResponseEntity.ok(true);
		}
		return ResponseEntity.notFound().build();
	}	
}
