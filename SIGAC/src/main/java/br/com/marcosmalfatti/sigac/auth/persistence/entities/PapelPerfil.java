/*
 * Copyright (C) 2022 Marcos Malfatti <marcos.malfatti@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package br.com.marcosmalfatti.sigac.auth.persistence.entities;

import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import br.com.marcosmalfatti.sigac.auth.persistence.SigacAbstractModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * @author Marcos Malfatti <marcos.malfatti@gmail.com>
 * @version 0.0.1
 * @date 2022-06
 */

@Entity
@Table(schema = "auth", name = "papel_perfil", indexes = {
		@Index(name = "ixpapel_perfil_IdPapel", columnList = "id_papel"),
		@Index(name = "ixpapel_perfil_IdPerfil", columnList = "id_perfil") }, uniqueConstraints = @UniqueConstraint(name = "ckpapel_perfil_Ids", columnNames = {
				"id_papel", "id_perfil" }))
@SequenceGenerator(schema = "auth", name = "seq_papel_perfil", sequenceName = "seq_papel_perfil", initialValue = 1, allocationSize = 1)
@NoArgsConstructor @SuperBuilder @Data @EqualsAndHashCode(callSuper = true)
public class PapelPerfil extends SigacAbstractModel<PapelPerfil> {

	private static final long serialVersionUID = -2872073642787106323L;

	@ManyToOne
	@JoinColumn(name = "id_papel", referencedColumnName = "id", foreignKey = @ForeignKey(name = "fkPapelPerfil_idPapel"))
	private Papel papel;

	@ManyToOne
	@JoinColumn(name = "id_perfil", referencedColumnName = "id", foreignKey = @ForeignKey(name = "fkPapelPerfil_idPerfil"))
	private Perfil perfil;

	@Override
	public PapelPerfil clone() {
		return PapelPerfil.builder()
				.id(getId())
				.ativo(getAtivo())
				.deleted(getDeleted())
				.papel(getPapel().clone())
				.perfil(getPerfil().clone())
				.build();
	}

	@Override
	public void copyFrom(PapelPerfil model) {
		if (model.getAtivo() != null) setAtivo(model.getAtivo());
		if (model.getPapel() != null) setPapel(model.getPapel().clone());
		if (model.getPerfil() != null) setPerfil(model.getPerfil().clone());
	}
}
