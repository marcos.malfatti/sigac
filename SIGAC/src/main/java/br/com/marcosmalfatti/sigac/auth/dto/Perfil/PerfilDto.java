/*
 * Copyright (C) 2022 Marcos Malfatti <marcos.malfatti@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package br.com.marcosmalfatti.sigac.auth.dto.Perfil;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import br.com.marcosmalfatti.sigac.auth.dto.AbstractDto;
import br.com.marcosmalfatti.sigac.auth.dto.Empresa.EmpresaDto;
import br.com.marcosmalfatti.sigac.auth.persistence.entities.Perfil;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
* @author Marcos Malfatti <marcos.malfatti@gmail.com>
* @version 0.0.1
* @date 2022-06
*/

@NoArgsConstructor @SuperBuilder @Data @EqualsAndHashCode(callSuper = true)
public class PerfilDto extends AbstractDto<Perfil> {
	
	@NotBlank
    private String nome;
	
	@NotNull
	@JsonProperty("empresa")
    private EmpresaDto empresaDto;
    
    public PerfilDto(Perfil perfil) {
    	fromModel(perfil);
    }
	
	public Perfil toModel() {
		return Perfil.builder()
				.id(getId())
				.nome(getNome())
				.ativo(getAtivo())
				.empresa(getEmpresaDto().toModel())
				.build();
    }

	@Override
	public void fromModel(Perfil model) {
		setId(model.getId());
		setNome(model.getNome());
		setEmpresaDto(new EmpresaDto(model.getEmpresa().clone()));
        setAtivo(model.getAtivo());
	}

}
