/*
 * Copyright (C) 2022 Marcos Malfatti <marcos.malfatti@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package br.com.marcosmalfatti.sigac.auth.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.marcosmalfatti.sigac.auth.dto.Papel.PapelDto;
import br.com.marcosmalfatti.sigac.auth.dto.Perfil.PerfilDto;
import br.com.marcosmalfatti.sigac.auth.persistence.entities.Papel;
import br.com.marcosmalfatti.sigac.auth.persistence.entities.PapelPerfil;
import br.com.marcosmalfatti.sigac.auth.persistence.entities.Perfil;
import br.com.marcosmalfatti.sigac.auth.persistence.repositories.RepPapel;
import br.com.marcosmalfatti.sigac.auth.persistence.repositories.RepPapelPerfil;
import br.com.marcosmalfatti.sigac.auth.persistence.repositories.RepPerfil;


/**
* @author Marcos Malfatti <marcos.malfatti@gmail.com>
* @version 0.0.1
* @date 2022-06
*/

@Service
@Transactional(
		readOnly = true,
		propagation = Propagation.REQUIRED)
public class SrvPapelPerfil {
	
    
    @Autowired
    private RepPapelPerfil repPapelPerfil;
    
    @Autowired
    private RepPerfil repPerfil;
    
    @Autowired
    private RepPapel repPapel;

    public void listarPapeis(Long perfilId) {
    	// TODO 
    }
    
    public void listarPapeis(PerfilDto perfilDto) {
    	// TODO 
    }
    
    public void adicionarPapel(PerfilDto perfilDto, PapelDto papelDto) {
    	
    	Perfil perfil = repPerfil.getById(perfilDto.getId());
    	Papel papel = repPapel.getById(papelDto.getId());
    	PapelPerfil papelPerfil = PapelPerfil.builder()
    								.papel(papel)
    								.perfil(perfil)
    								.build();
    	repPapelPerfil.save(papelPerfil);
    }
    
    public void adicionarPapel(Long perfilId, Long papelId) {
    	// TODO 
    }
    
    public void removerPapel(PerfilDto perfilDto, PapelDto papelDto) {
    	// TODO 
    }
    
    public void removerPapel(Long perfilId, Long papelId) {
    	// TODO 
    }
    
	public void listarPerfis() {
		// TODO 
	}
	
	public void adicionarPerfil() {
		// TODO 
	}
	
	public void removerPerfil() {
		// TODO 
	}
}
