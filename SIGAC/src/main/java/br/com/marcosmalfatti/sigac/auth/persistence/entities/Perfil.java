/*
 * Copyright (C) 2022 Marcos Malfatti <marcos.malfatti@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package br.com.marcosmalfatti.sigac.auth.persistence.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import br.com.marcosmalfatti.sigac.auth.persistence.SigacAbstractModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * @author Marcos Malfatti <marcos.malfatti@gmail.com>
 * @version 0.0.1
 * @date 2022-06
 */

@Entity
@Table(
	schema = "auth", 
	name = "perfil", 
	indexes = { 
		@Index(name = "ixPerfil_Nome", columnList = "nome"),
		@Index(name = "ixPerfil_Ativo", columnList = "ativo"),
		@Index(name = "ixPerfil_Deleted", columnList = "deleted") })
@SQLDelete(sql = "UPDATE auth.perfil SET deleted = true WHERE id=?")
@Where(clause = "deleted=false")
@SequenceGenerator(schema = "auth", name = "seq_perfil", sequenceName = "seq_perfil", initialValue = 1, allocationSize = 1)
@NoArgsConstructor @SuperBuilder @Data @EqualsAndHashCode(callSuper = true)
public class Perfil extends SigacAbstractModel<Perfil> {

	private static final long serialVersionUID = -8306352652145486069L;

	@Column(name = "nome", nullable = false)
	private String nome;

	@ManyToOne
	@JoinColumn(name = "id_empresa", referencedColumnName = "id", foreignKey = @ForeignKey(name = "fkPapel_IdEmpresa"))
	private Empresa empresa;

	@Override
	public Perfil clone() {
		return Perfil.builder()
				.id(getId())
				.nome(getNome())
				.ativo(getAtivo())
				.empresa(getEmpresa().clone())
				.deleted(getDeleted())
				.build();
	}

	@Override
	public void copyFrom(Perfil model) {
		if (model.getNome() != null) setNome(model.getNome());
		if (model.getAtivo() != null) setAtivo(model.getAtivo());
		if (model.getEmpresa() != null) setEmpresa(model.getEmpresa().clone());
	}

}
