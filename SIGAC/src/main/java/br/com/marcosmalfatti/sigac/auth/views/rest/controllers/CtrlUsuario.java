/*
 * Copyright (C) 2022 Marcos Malfatti <marcos.malfatti@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package br.com.marcosmalfatti.sigac.auth.views.rest.controllers;

import java.net.URI;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.marcosmalfatti.sigac.auth.dto.Ordenacao;
import br.com.marcosmalfatti.sigac.auth.dto.Paginacao;
import br.com.marcosmalfatti.sigac.auth.dto.Empresa.EmpresaDto;
import br.com.marcosmalfatti.sigac.auth.dto.UnidadeOrganizacional.UnidadeOrganizacionalDto;
import br.com.marcosmalfatti.sigac.auth.dto.Usuario.UsuarioDto;
import br.com.marcosmalfatti.sigac.auth.dto.Usuario.UsuarioFormLstDto;
import br.com.marcosmalfatti.sigac.auth.service.SrvUsuario;

/**
* @author Marcos Malfatti <marcos.malfatti@gmail.com>
* @version 0.0.1
* @date 2022-06
*/

@RestController
@RequestMapping("api/v1/Usuarios")
public class CtrlUsuario {

	@Autowired
	private SrvUsuario srvUsuario;
	
	@GetMapping(path = "exemplo")
	public ResponseEntity<?> exemplo(){
		EmpresaDto empresa = EmpresaDto.builder()
				.id(1L)
				.nome("Empresa Exemplo")
				.ativo(true)
				.build();
		UnidadeOrganizacionalDto unidadeOrganizacional = UnidadeOrganizacionalDto.builder()
				.nome("Exemplo")
				.empresaDto(empresa)
				.build();
		UsuarioFormLstDto retorno = UsuarioFormLstDto.builder()
									.login("Exemplo")
									.senha("senha")
									.empresaDto(empresa)
									.unidadeOrganizacionalDto(unidadeOrganizacional)
									.ativo(Boolean.TRUE)
									.parcial(Boolean.FALSE)
									.paginacao(Paginacao.builder()
											.tamanhoPagina(15)
											.pagina(1)
											.build())
									.ordenacao(Ordenacao.builder().build())
									.build();
		return ResponseEntity.ok(retorno);
	}
	
	@GetMapping(path = "porId/{usuarioId}")
	public ResponseEntity<?> getUsuarioPorId(@PathVariable("usuarioId") Long usuarioId) {
		Optional<UsuarioDto> usuarioDtoOpt = srvUsuario.buscarPorId(usuarioId);
		if (usuarioDtoOpt.isPresent()) {
			return ResponseEntity.ok(usuarioDtoOpt.get());
		}
		return ResponseEntity.notFound().build();
	}
	
	@GetMapping(path = "listaUsuarios")
	public ResponseEntity<?> listaUsuarios(@RequestBody @Valid UsuarioFormLstDto usuarioFormLstDto) {
		Page<UsuarioDto> usuarioDtoPg = srvUsuario.listar(usuarioFormLstDto);
		if (usuarioDtoPg.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(usuarioDtoPg);
	}
	
	@PostMapping(path = "adicionar")
	public ResponseEntity<?> adicionaUsuario(@RequestBody @Valid UsuarioDto usuarioDto,
			UriComponentsBuilder uriBuilder) {
		usuarioDto = srvUsuario.adicionar(usuarioDto);
		URI uri = uriBuilder.path("api/v1/Usuarios/{id}").buildAndExpand(usuarioDto.getId()).toUri();
		return ResponseEntity.created(uri).body(usuarioDto);
	}
	
	@PutMapping(path = "alterar/{usuarioId}")
	public ResponseEntity<?> alteraUsuario(@PathVariable("usuarioId") Long usuarioId, @RequestBody @Valid UsuarioDto usuarioDto) {
		Optional<UsuarioDto> usuarioDtoOpt = srvUsuario.alterar(usuarioId, usuarioDto);
		if (usuarioDtoOpt.isPresent()) {
			return ResponseEntity.ok(usuarioDtoOpt.get());
		}
		return ResponseEntity.notFound().build();
	}
	
	@PutMapping(path = "ativar/{usuarioId}")
	public ResponseEntity<?> ativaUsuario(@PathVariable("usuarioId") Long usuarioId) {
		Optional<UsuarioDto> usuarioDtoOpt = srvUsuario.ativar(usuarioId);
		if (usuarioDtoOpt.isPresent()) {
			return ResponseEntity.ok(usuarioDtoOpt.get());
		}
		return ResponseEntity.notFound().build();
	}
	
	@PutMapping(path = "desativar/{usuarioId}")
	public ResponseEntity<?> desativaUsuario(@PathVariable("usuarioId") Long usuarioId) {
		Optional<UsuarioDto> usuarioDtoOpt = srvUsuario.desativar(usuarioId);
		if (usuarioDtoOpt.isPresent()) {
			return ResponseEntity.ok(usuarioDtoOpt.get());
		}
		return ResponseEntity.notFound().build();
	}
	
	@DeleteMapping(path = "{usuarioId}")
	public ResponseEntity<?> eliminaUsuario(@PathVariable("usuarioId") Long usuarioId) {
		if (srvUsuario.eliminar(usuarioId)) {
			return ResponseEntity.ok(true);
		}
		return ResponseEntity.notFound().build();
	}
}
