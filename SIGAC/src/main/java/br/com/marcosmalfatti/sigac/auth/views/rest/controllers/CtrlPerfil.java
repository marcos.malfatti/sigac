/*
 * Copyright (C) 2022 Marcos Malfatti <marcos.malfatti@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package br.com.marcosmalfatti.sigac.auth.views.rest.controllers;

import java.net.URI;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.marcosmalfatti.sigac.auth.dto.Ordenacao;
import br.com.marcosmalfatti.sigac.auth.dto.Paginacao;
import br.com.marcosmalfatti.sigac.auth.dto.Empresa.EmpresaDto;
import br.com.marcosmalfatti.sigac.auth.dto.Perfil.PerfilDto;
import br.com.marcosmalfatti.sigac.auth.dto.Perfil.PerfilFormLstDto;
import br.com.marcosmalfatti.sigac.auth.service.SrvPerfil;

/**
* @author Marcos Malfatti <marcos.malfatti@gmail.com>
* @version 0.0.1
* @date 2022-06
*/

@RestController
@RequestMapping("api/v1/Perfis")
public class CtrlPerfil {
	@Autowired
	private SrvPerfil srvPerfil;

	@GetMapping(path="exemplo")
	public ResponseEntity<?> exemplo(){
		EmpresaDto empresa = EmpresaDto.builder()
								.id(1L)
								.nome("Empresa Exemplo")
								.ativo(true)
								.build();
		PerfilFormLstDto retorno = PerfilFormLstDto.builder()
									.nome("Exemplo")
									.empresaDto(empresa)
									.ativo(true)
									.parcial(false)
									.paginacao(Paginacao.builder()
												.tamanhoPagina(15)
												.pagina(1)
												.build())
								.ordenacao(Ordenacao.builder().build())
								.build();
		return ResponseEntity.ok(retorno);
	}
	
	@GetMapping(path = "{perfilId}")
	public ResponseEntity<?> getPerfilPorId(@PathVariable("perfilId") Long perfilId) {
		Optional<PerfilDto> perfilDtoOpt = srvPerfil.buscarPorId(perfilId);
		if (perfilDtoOpt.isPresent()) {
			return ResponseEntity.ok(perfilDtoOpt.get());
		}
		return ResponseEntity.notFound().build();
	}

	@GetMapping(path = "listaPerfis")
	public ResponseEntity<?> listaPerfis(@RequestBody @Valid PerfilFormLstDto perfilFormLstDto) {
		Page<PerfilDto> perfilDtoOpt = srvPerfil.listar(perfilFormLstDto);
		if (perfilDtoOpt.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(perfilDtoOpt);
	}

	@PostMapping(path = "adicionar")
	public ResponseEntity<?> adicionaPerfil(@RequestBody @Valid PerfilDto perfilDto,
			UriComponentsBuilder uriBuilder) {
		perfilDto = srvPerfil.adicionar(perfilDto);
		URI uri = uriBuilder.path("api/v1/Perfis/{id}").buildAndExpand(perfilDto.getId()).toUri();
		return ResponseEntity.created(uri).body(perfilDto);
	}

	@PutMapping(path = "alterar/{perfilId}")
	public ResponseEntity<?> alteraPerfil(@PathVariable("perfilId") Long perfilId, @RequestBody @Valid PerfilDto perfilDto) {
		Optional<PerfilDto> perfilDtoOpt = srvPerfil.alterar(perfilId, perfilDto);
		if (perfilDtoOpt.isPresent()) {
			return ResponseEntity.ok(perfilDtoOpt.get());
		}
		return ResponseEntity.notFound().build();
	}

	@PutMapping(path = "ativar/{perfilId}")
	public ResponseEntity<?> ativaPerfil(@PathVariable("perfilId") Long perfilId) {
		Optional<PerfilDto> perfilDtoOpt = srvPerfil.ativar(perfilId);
		if (perfilDtoOpt.isPresent()) {
			return ResponseEntity.ok(perfilDtoOpt.get());
		}
		return ResponseEntity.notFound().build();
	}

	@PutMapping(path = "desativar/{perfilId}")
	public ResponseEntity<?> desativaPerfil(@PathVariable("perfilId") Long perfilId) {
		Optional<PerfilDto> perfilDtoOpt = srvPerfil.desativar(perfilId);
		if (perfilDtoOpt.isPresent()) {
			return ResponseEntity.ok(perfilDtoOpt.get());
		}
		return ResponseEntity.notFound().build();
	}

	@DeleteMapping(path = "{perfilId}")
	public ResponseEntity<?> eliminaPerfil(@PathVariable("perfilId") Long perfilId) {
		if (srvPerfil.eliminar(perfilId)) {
			return ResponseEntity.ok(true);
		}
		return ResponseEntity.notFound().build();
	}
}
