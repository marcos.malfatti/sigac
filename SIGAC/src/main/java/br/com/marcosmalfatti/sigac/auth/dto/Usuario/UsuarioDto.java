/*
 * Copyright (C) 2022 Marcos Malfatti <marcos.malfatti@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package br.com.marcosmalfatti.sigac.auth.dto.Usuario;

import java.time.LocalDateTime;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import br.com.marcosmalfatti.sigac.auth.dto.AbstractDto;
import br.com.marcosmalfatti.sigac.auth.dto.Empresa.EmpresaDto;
import br.com.marcosmalfatti.sigac.auth.dto.UnidadeOrganizacional.UnidadeOrganizacionalDto;
import br.com.marcosmalfatti.sigac.auth.persistence.entities.Usuario;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
* @author Marcos Malfatti <marcos.malfatti@gmail.com>
* @version 0.0.1
* @date 2022-06
*/

@NoArgsConstructor @SuperBuilder @Data @EqualsAndHashCode(callSuper = true)
public class UsuarioDto extends AbstractDto<Usuario>{

    @NotBlank
    private String login;

    @NotBlank
    private String senha;

    @NotNull
    @JsonProperty("empresa")
    private EmpresaDto empresaDto;
    
    @JsonProperty("unidadeOrganizacional")
    private UnidadeOrganizacionalDto unidadeOrganizacionalDto;

    private LocalDateTime ultimaAtualizacao;

    public UsuarioDto(Usuario usuario) {
    	fromModel(usuario);
    }

	@Override
	public void fromModel(Usuario model) {
		setId(model.getId());
		setLogin(model.getLogin());
		setSenha(model.getSenha());
		setEmpresaDto(new EmpresaDto(model.getEmpresa().clone()));
		setUltimaAtualizacao(model.getUltimaAtualizacao());
		setAtivo(model.getAtivo());
		
	}

	@Override
	public Usuario toModel() {
		return Usuario.builder()
				.id(getId())
				.login(getLogin())
				.senha(getSenha())
				.empresa(getEmpresaDto().toModel().clone())
				.unidade(getUnidadeOrganizacionalDto().toModel().clone())
				.ultimaAtualizacao(getUltimaAtualizacao())
				.ativo(getAtivo())
				.build();
	}
}
