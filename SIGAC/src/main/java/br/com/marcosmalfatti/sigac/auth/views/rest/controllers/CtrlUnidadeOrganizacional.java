/*
 * Copyright (C) 2022 Marcos Malfatti <marcos.malfatti@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package br.com.marcosmalfatti.sigac.auth.views.rest.controllers;

import java.net.URI;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.marcosmalfatti.sigac.auth.dto.Ordenacao;
import br.com.marcosmalfatti.sigac.auth.dto.Paginacao;
import br.com.marcosmalfatti.sigac.auth.dto.Empresa.EmpresaDto;
import br.com.marcosmalfatti.sigac.auth.dto.UnidadeOrganizacional.UnidadeOrganizacionalDto;
import br.com.marcosmalfatti.sigac.auth.dto.UnidadeOrganizacional.UnidadeOrganizacionalFormLstDto;
import br.com.marcosmalfatti.sigac.auth.service.SrvUnidadeOrganizacional;

/**
* @author Marcos Malfatti <marcos.malfatti@gmail.com>
* @version 0.0.1
* @date 2022-06
*/

@RestController
@RequestMapping("api/v1/UnidadesOrganizacionais")
public class CtrlUnidadeOrganizacional {
	
	@Autowired
    private SrvUnidadeOrganizacional srvUnidadeOrganizacional;
	
	@GetMapping(path="exemplo")
	public ResponseEntity<?> exemplo(){
		UnidadeOrganizacionalFormLstDto retorno = UnidadeOrganizacionalFormLstDto.builder()
										.nome("Exemplo")
										.empresaDto(EmpresaDto.builder()
														.id(1L)
														.ativo(Boolean.TRUE)
														.nome("Empresa de Exemplo 01")
														.build())
										.ativo(true)
										.parcial(false)
										.paginacao(Paginacao.builder()
													.tamanhoPagina(15)
													.pagina(1)
													.build())
										.ordenacao(Ordenacao.builder().build())
										.build();
		return ResponseEntity.ok(retorno);
	}
	
	@GetMapping(path = "porId/{unidadeOrganizacionalId}")
	public ResponseEntity<?> getUnidadeOrganizacionalPorId(@PathVariable("unidadeOrganizacionalId") Long unidadeOrganizacionalId) {
		Optional<UnidadeOrganizacionalDto> unidadeOrganizacionalDtoOpt = srvUnidadeOrganizacional.buscarPorId(unidadeOrganizacionalId);
		if (unidadeOrganizacionalDtoOpt.isPresent()) {
			return ResponseEntity.ok(unidadeOrganizacionalDtoOpt.get());
		}
		return ResponseEntity.notFound().build();
	}
	
	@GetMapping(path = "listaUnidadeOrganizacionals")
	public ResponseEntity<?> listaUnidadeOrganizacionals(@RequestBody @Valid UnidadeOrganizacionalFormLstDto unidadeOrganizacionalFormLstDto) {
		Page<UnidadeOrganizacionalDto> unidadeOrganizacionalDtoPg = srvUnidadeOrganizacional.listar(unidadeOrganizacionalFormLstDto);
		if (unidadeOrganizacionalDtoPg.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(unidadeOrganizacionalDtoPg);
	}
	
	@PostMapping(path = "adicionar")
	public ResponseEntity<?> adicionaUnidadeOrganizacional(@RequestBody @Valid UnidadeOrganizacionalDto unidadeOrganizacionalDto,
			UriComponentsBuilder uriBuilder) {
		unidadeOrganizacionalDto = srvUnidadeOrganizacional.adicionar(unidadeOrganizacionalDto);
		URI uri = uriBuilder.path("api/v1/UnidadesOrganizacionais/{id}").buildAndExpand(unidadeOrganizacionalDto.getId()).toUri();
		return ResponseEntity.created(uri).body(unidadeOrganizacionalDto);
	}
	
	@PutMapping(path = "alterar/{unidadeOrganizacionalId}")
	public ResponseEntity<?> alteraUnidadeOrganizacional(@PathVariable("unidadeOrganizacionalId") Long unidadeOrganizacionalId, @RequestBody @Valid UnidadeOrganizacionalDto unidadeOrganizacionalDto) {
		Optional<UnidadeOrganizacionalDto> unidadeOrganizacionalDtoOpt = srvUnidadeOrganizacional.alterar(unidadeOrganizacionalId, unidadeOrganizacionalDto);
		if (unidadeOrganizacionalDtoOpt.isPresent()) {
			return ResponseEntity.ok(unidadeOrganizacionalDtoOpt.get());
		}
		return ResponseEntity.notFound().build();
	}
	
	@PutMapping(path = "ativar/{unidadeOrganizacionalId}")
	public ResponseEntity<?> ativaUnidadeOrganizacional(@PathVariable("unidadeOrganizacionalId") Long unidadeOrganizacionalId) {
		Optional<UnidadeOrganizacionalDto> unidadeOrganizacionalDtoOpt = srvUnidadeOrganizacional.ativar(unidadeOrganizacionalId);
		if (unidadeOrganizacionalDtoOpt.isPresent()) {
			return ResponseEntity.ok(unidadeOrganizacionalDtoOpt.get());
		}
		return ResponseEntity.notFound().build();
	}
	
	@PutMapping(path = "desativar/{unidadeOrganizacionalId}")
	public ResponseEntity<?> desativaUnidadeOrganizacional(@PathVariable("unidadeOrganizacionalId") Long unidadeOrganizacionalId) {
		Optional<UnidadeOrganizacionalDto> unidadeOrganizacionalDtoOpt = srvUnidadeOrganizacional.desativar(unidadeOrganizacionalId);
		if (unidadeOrganizacionalDtoOpt.isPresent()) {
			return ResponseEntity.ok(unidadeOrganizacionalDtoOpt.get());
		}
		return ResponseEntity.notFound().build();
	}
	
	@DeleteMapping(path = "{unidadeOrganizacionalId}")
	public ResponseEntity<?> eliminaUnidadeOrganizacional(@PathVariable("unidadeOrganizacionalId") Long unidadeOrganizacionalId) {
		if (srvUnidadeOrganizacional.eliminar(unidadeOrganizacionalId)) {
			return ResponseEntity.ok(true);
		}
		return ResponseEntity.notFound().build();
	}
}
