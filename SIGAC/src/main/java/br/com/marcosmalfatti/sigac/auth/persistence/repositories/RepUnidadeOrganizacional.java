/*
 * Copyright (C) 2022 Marcos Malfatti <marcos.malfatti@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package br.com.marcosmalfatti.sigac.auth.persistence.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;
import org.springframework.stereotype.Repository;

import br.com.marcosmalfatti.sigac.auth.persistence.entities.UnidadeOrganizacional;

/**
* @author Marcos Malfatti <marcos.malfatti@gmail.com>
* @version 0.0.1
* @date 2022-06
*/

@Repository
public interface RepUnidadeOrganizacional extends JpaRepository<UnidadeOrganizacional, Long>, QueryByExampleExecutor<UnidadeOrganizacional>{

	public Optional<UnidadeOrganizacional> findByNome(String nome);

    public Boolean existsByNome(String nome);

    public List<UnidadeOrganizacional> findByAtivoTrue();

    public List<UnidadeOrganizacional> findByAtivoFalse();

    public List<UnidadeOrganizacional> findByNomeContainsIgnoreCase(String nome);
}
