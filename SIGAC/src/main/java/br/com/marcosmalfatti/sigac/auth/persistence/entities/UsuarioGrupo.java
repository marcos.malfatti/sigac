/*
 * Copyright (C) 2022 Marcos Malfatti <marcos.malfatti@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package br.com.marcosmalfatti.sigac.auth.persistence.entities;

import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import br.com.marcosmalfatti.sigac.auth.persistence.SigacAbstractModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * @author Marcos Malfatti <marcos.malfatti@gmail.com>
 * @version 0.0.1
 * @date 2022-06
 */

@Entity
@Table(schema = "auth", name = "usuario_grupo", indexes = {
		@Index(name = "ixUsuarioGrupo_IdUsuario", columnList = "id_usuario"),
		@Index(name = "ixUsuarioGrupo_IdGrupo", columnList = "id_grupo") }, uniqueConstraints = @UniqueConstraint(name = "ckUsuarioGrupo_Ids", columnNames = {
				"id_usuario", "id_grupo" }))
@SequenceGenerator(schema = "auth", name = "seq_usuario_grupo", sequenceName = "seq_usuario_grupo", initialValue = 1, allocationSize = 1)
@NoArgsConstructor @SuperBuilder @Data @EqualsAndHashCode(callSuper = true)
public class UsuarioGrupo extends SigacAbstractModel<UsuarioGrupo> {

	private static final long serialVersionUID = 2441671842508426503L;

	@ManyToOne
	@JoinColumn(name = "id_usuario", referencedColumnName = "id", foreignKey = @ForeignKey(name = "fkUsuarioGrupo_idUsuario"))
	private Usuario usuario;

	@ManyToOne
	@JoinColumn(name = "id_grupo", referencedColumnName = "id", foreignKey = @ForeignKey(name = "fkUsuarioGrupo_idGrupo"))
	private Grupo grupo;

	@Override
	public UsuarioGrupo clone() {
		return UsuarioGrupo.builder()
				.id(getId())
				.ativo(getAtivo())
				.deleted(getDeleted())
				.usuario(getUsuario().clone())
				.grupo(getGrupo().clone())
				.build();
	}

	@Override
	public void copyFrom(UsuarioGrupo model) {
		if (model.getAtivo() != null) setAtivo(model.getAtivo());
		if (model.getGrupo() != null) setGrupo(model.getGrupo().clone());
		if (model.getUsuario() != null) setUsuario(model.getUsuario().clone());
	}

}
