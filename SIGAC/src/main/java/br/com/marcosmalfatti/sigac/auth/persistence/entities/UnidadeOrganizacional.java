/*
 * Copyright (C) 2022 Marcos Malfatti <marcos.malfatti@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package br.com.marcosmalfatti.sigac.auth.persistence.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import br.com.marcosmalfatti.sigac.auth.persistence.SigacAbstractModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * @author Marcos Malfatti <marcos.malfatti@gmail.com>
 * @version 0.0.1
 * @date 2022-06
 */

@Entity
@Table(
	schema = "auth", 
	name = "unidade_organizacional", 
	indexes = {
		@Index(name = "ixUnidadeOrganizacional__IdEmpresa", columnList = "id_empresa"),
		@Index(name = "ixUnidadeOrganizacional_Deleted", columnList = "deleted"),
		@Index(name = "ixUnidadeOrganizacional_Nome", columnList = "nome") })
@SQLDelete(sql = "UPDATE auth.unidade_organizacional SET deleted = true WHERE id = ?")
@Where(clause = "deleted = false")
@SequenceGenerator(schema = "auth", name = "seq_unidade_organizcional", sequenceName = "seq_unidade_organizcional", initialValue = 1, allocationSize = 1)
@NoArgsConstructor @SuperBuilder @Data @EqualsAndHashCode(callSuper = true)
public class UnidadeOrganizacional extends SigacAbstractModel<UnidadeOrganizacional> {

	private static final long serialVersionUID = 3421024941219194742L;

	@Column(name = "nome", nullable = false)
	private String nome;

	@ManyToOne
	@JoinColumn(name = "id_empresa", referencedColumnName = "id", foreignKey = @ForeignKey(name = "fkGrupo_IdEmpresa"))
	private Empresa empresa;

	@Override
	public UnidadeOrganizacional clone() {
		return UnidadeOrganizacional.builder()
				.id(getId())
				.nome(getNome())
				.ativo(getAtivo())
				.empresa(getEmpresa().clone())
				.deleted(getDeleted())
				.build();
	}

	@Override
	public void copyFrom(UnidadeOrganizacional model) {
		if (model.getNome() != null) setNome(model.getNome());
		if (model.getAtivo()!= null) setAtivo(model.getAtivo());
		if (model.getEmpresa() != null) setEmpresa( model.getEmpresa().clone());
	}

}
