/*
 * Copyright (C) 2022 Marcos Malfatti <marcos.malfatti@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package br.com.marcosmalfatti.sigac.auth.views.rest.controllers;

import java.net.URI;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.marcosmalfatti.sigac.auth.dto.Ordenacao;
import br.com.marcosmalfatti.sigac.auth.dto.Paginacao;
import br.com.marcosmalfatti.sigac.auth.dto.Empresa.EmpresaDto;
import br.com.marcosmalfatti.sigac.auth.dto.Empresa.EmpresaFormLstDto;
import br.com.marcosmalfatti.sigac.auth.service.SrvEmpresa;

/**
* @author Marcos Malfatti <marcos.malfatti@gmail.com>
* @version 0.0.1
* @date 2022-06
*/

@RestController
@RequestMapping("api/v1/Empresas")
public class CtrlEmpresa {
	@Autowired
    private SrvEmpresa srvEmpresa;

	@GetMapping(path="exemplo")
	public ResponseEntity<?> exemplo(){
		EmpresaFormLstDto retorno = EmpresaFormLstDto.builder()
										.nome("Exemplo")
										.ativo(true)
										.parcial(false)
										.paginacao(Paginacao.builder()
													.tamanhoPagina(15)
													.pagina(1)
													.build())
										.ordenacao(Ordenacao.builder().build())
										.build();
		return ResponseEntity.ok(retorno);
	}
	
	@GetMapping(path = "{empresaId}")
	public ResponseEntity<?> getEmpresaPorId(@PathVariable("empresaId") Long empresaId) {
		Optional<EmpresaDto> empresaDtoOpt = srvEmpresa.buscarPorId(empresaId);
		if (empresaDtoOpt.isPresent()) {
			return ResponseEntity.ok(empresaDtoOpt.get());
		}
		return ResponseEntity.notFound().build();
	}
	
	@GetMapping(path = "listaEmpresas")
	public ResponseEntity<?> listaEmpresas(@RequestBody @Valid EmpresaFormLstDto empresaFormLstDto) {
		Page<EmpresaDto> empresaDtoPg = srvEmpresa.listar(empresaFormLstDto);
		if (empresaDtoPg.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(empresaDtoPg);
	}
	
	@PostMapping(path = "adicionar")
	public ResponseEntity<?> adicionaEmpresa(@RequestBody @Valid EmpresaDto empresaDto,
			UriComponentsBuilder uriBuilder) {
		empresaDto = srvEmpresa.adicionar(empresaDto);
		URI uri = uriBuilder.path("api/v1/Empresas/{id}").buildAndExpand(empresaDto.getId()).toUri();
		return ResponseEntity.created(uri).body(empresaDto);
	}
	
	@PutMapping(path = "alterar/{empresaId}")
	public ResponseEntity<?> alteraEmpresa(@PathVariable("empresaId") Long empresaId, @RequestBody @Valid EmpresaDto empresaDto) {
		Optional<EmpresaDto> empresaDtoOpt = srvEmpresa.alterar(empresaId, empresaDto);
		if (empresaDtoOpt.isPresent()) {
			return ResponseEntity.ok(empresaDtoOpt.get());
		}
		return ResponseEntity.notFound().build();
	}
	
	@PutMapping(path = "ativar/{empresaId}")
	public ResponseEntity<?> ativaEmpresa(@PathVariable("empresaId") Long empresaId) {
		Optional<EmpresaDto> empresaDtoOpt = srvEmpresa.ativar(empresaId);
		if (empresaDtoOpt.isPresent()) {
			return ResponseEntity.ok(empresaDtoOpt.get());
		}
		return ResponseEntity.notFound().build();
	}
	
	@PutMapping(path = "desativar/{empresaId}")
	public ResponseEntity<?> desativaEmpresa(@PathVariable("empresaId") Long empresaId) {
		Optional<EmpresaDto> empresaDtoOpt = srvEmpresa.desativar(empresaId);
		if (empresaDtoOpt.isPresent()) {
			return ResponseEntity.ok(empresaDtoOpt.get());
		}
		return ResponseEntity.notFound().build();
	}
	
	@DeleteMapping(path = "{empresaId}")
	public ResponseEntity<?> eliminaEmpresa(@PathVariable("empresaId") Long empresaId) {
		if (srvEmpresa.eliminar(empresaId)) {
			return ResponseEntity.ok(true);
		}
		return ResponseEntity.notFound().build();
	}
}
